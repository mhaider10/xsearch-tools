#include <vector>
#include <iostream>
#include <numeric>
#include <algorithm>
#include <string.h>
#include <math.h>

#define DELIMS " \t\n/_.,;:"
#define MSG "Workload Stats -- Pass in file which lists all target files\n\
Target files should be of same length\n\
./workload_stats <filename> <size of each file>"

typedef struct {
    double average_token_length;
    double stddev_token_length;
} WorkloadData;

WorkloadData get_sample_stats(char *filename, unsigned long filesize, char *buffer, std::vector<size_t>& all_token_lengths);
bool read_filenames(const std::string& file_name, std::vector<char*>& filenames);
size_t find_median_indices(const std::vector<size_t>& elements, size_t low, size_t high, size_t *median_indices);
size_t avg(size_t *elems, size_t num_elems);

int main(int argc, char **argv) {
    if (argc < 3) {
        std::cout << MSG << std::endl;
        return 1;
    }

    std::string file_name = std::string(argv[1]);
    unsigned long file_size = std::atol(argv[2]);

    std::vector<char*> filenames;
    bool filenames_success = read_filenames(file_name, filenames);
    if (!filenames_success) return 1;

    std::vector<size_t> all_token_lengths;
    std::vector<double> means;
    std::vector<double> stddevs;
    means.reserve(filenames.size());
    stddevs.reserve(filenames.size());
    char *read_buffer = (char*) malloc(file_size + 1024);
    for (auto filename_ptr = filenames.begin(); filename_ptr != filenames.end(); filename_ptr++) {
        WorkloadData data = get_sample_stats(*filename_ptr, file_size, read_buffer, all_token_lengths);
        means.push_back(data.average_token_length);
        stddevs.push_back(data.stddev_token_length);
    }

    std::sort(all_token_lengths.begin(), all_token_lengths.end());
    std::cout << "Total tokens: " << all_token_lengths.size() << std::endl;

    size_t min_token_length = all_token_lengths[0];
    size_t max_token_length = all_token_lengths[all_token_lengths.size() - 1];

    size_t median_indices_buffer[2]{0, 0};
    size_t median_elements_buffer[2]{0, 0};
    size_t median_idxs_count = find_median_indices(all_token_lengths, 0, all_token_lengths.size() - 1, median_indices_buffer);
    median_elements_buffer[0] = all_token_lengths[median_indices_buffer[0]];
    if (median_idxs_count == 2) median_elements_buffer[1] = all_token_lengths[median_indices_buffer[1]];
    size_t median_low_index = median_indices_buffer[0];
    size_t median_high_index = median_indices_buffer[1];
    size_t token_length_median = avg(median_elements_buffer, median_idxs_count);

    size_t q1_upper_idx = std::max<size_t>(0, (median_idxs_count == 1) ? median_low_index - 1 : median_low_index);
    size_t q1_idxs_count = find_median_indices(all_token_lengths, 0, q1_upper_idx, median_indices_buffer);
    median_elements_buffer[0] = all_token_lengths[median_indices_buffer[0]];
    if (q1_idxs_count == 2) median_elements_buffer[1] = all_token_lengths[median_indices_buffer[1]];
    size_t token_length_q1 = avg(median_elements_buffer, q1_idxs_count);

    size_t q2_lower_idx = std::min<size_t>(all_token_lengths.size() - 1, (median_idxs_count == 1) ? median_low_index + 1 : median_high_index);
    size_t q2_idxs_count = find_median_indices(all_token_lengths, q2_lower_idx, all_token_lengths.size() - 1, median_indices_buffer);
    median_elements_buffer[0] = all_token_lengths[median_indices_buffer[0]];
    if (q2_idxs_count == 2) median_elements_buffer[1] = all_token_lengths[median_indices_buffer[1]];
    size_t token_length_q2 = avg(median_elements_buffer, q2_idxs_count);

    double total_mean = ((double) std::accumulate(all_token_lengths.begin(), all_token_lengths.end(), 0)) / all_token_lengths.size();

    double total_stddev_ir = std::accumulate(all_token_lengths.begin(), all_token_lengths.end(), 0.0, [=] (const double& a, const size_t& b) -> double {
        return a + ((b - total_mean) * (b - total_mean));
    }) / all_token_lengths.size();
    double total_stddev = std::sqrt(total_stddev_ir / all_token_lengths.size());

    double average_mean = std::accumulate(means.begin(), means.end(), 0.0) / means.size();
    double average_stddev = std::accumulate(stddevs.begin(), stddevs.end(), 0.0) / stddevs.size();

    std::cout << "Total mean of all data: " << total_mean << std::endl;
    std::cout << "Total standard deviation of all data: " << total_stddev << std::endl;

    std::cout << std::endl;

    std::cout << "Min token length of all data: " << min_token_length << std::endl;
    std::cout << "Q1 token length of all data: " << token_length_q1 << std::endl;
    std::cout << "Median token length of all data: " << token_length_median << std::endl;
    std::cout << "Q2 token length of all data: " << token_length_q2 << std::endl;
    std::cout << "Max token length of all data: " << max_token_length << std::endl;

    std::cout << std::endl;

    std::cout << "Total standard deviation of " << filenames.size() << " samples: " << total_stddev << std::endl;
    std::cout << "Average mean of " << filenames.size() << " samples: " << average_mean << std::endl;
    std::cout << "Average standard deviation of " << filenames.size() << " samples: " << average_stddev << std::endl;

}

WorkloadData get_sample_stats(char *filename, unsigned long filesize, char *buffer, std::vector<size_t>& all_token_lengths) {
    FILE *fp = fopen(filename, "r");
    if (fp == NULL) {
        std::cout << "ERR: cannot open file " << filename << std::endl;
        return { .average_token_length = 0, .stddev_token_length = 0 };
    }
    memset(buffer, 0, filesize + 1024);

    size_t bytes_remaining = filesize;
    
    while (bytes_remaining > 0) {
        size_t members_read = fread(buffer, filesize, 1, fp);
        size_t bytes_read = members_read * filesize;
        bytes_remaining -= bytes_read;
        if (bytes_read == 0) break;
    }

    fflush(fp);
    char *current_token;
    char *saveptr = buffer;
    unsigned int total_tokens = 0;
    long double token_length = 0;
    std::vector<size_t> token_lengths;
    while ((current_token = strtok_r(saveptr, DELIMS, &saveptr)) != NULL) {
        size_t current_token_length = strlen(current_token);
        token_length += current_token_length;
        token_lengths.push_back(current_token_length);
        all_token_lengths.push_back(current_token_length);
        total_tokens++;
    }
    double token_length_average = token_length / total_tokens;
    double stddev_summation_term = 0;
    for (auto token_len_ptr = token_lengths.begin(); token_len_ptr != token_lengths.end(); token_len_ptr++) {
        double inner_term = (*token_len_ptr) - token_length_average;
        inner_term *= inner_term;
        stddev_summation_term += inner_term;
    }
    double stddev = stddev_summation_term / total_tokens;
    stddev = sqrt(stddev);
    return { .average_token_length = token_length_average, .stddev_token_length = stddev };
}

bool read_filenames(const std::string& file_name, std::vector<char*>& filenames)
{
    FILE *fp = fopen(file_name.c_str(), "r");
    if (fp == NULL) {
        std::cout << "ERR: cannot open file " << file_name << std::endl;
        return false;
    }

    char *rc, *line_buffer;
    while (true) {
        line_buffer = new char[4096];
        
        rc = fgets(line_buffer, 4096, fp);
        if (rc == NULL) {
            delete line_buffer;
            break;
        }

        // Strip new line
        if (line_buffer[strlen(line_buffer) - 1] == '\n') {
            line_buffer[strlen(line_buffer) - 1] = '\0';
        }

        filenames.push_back(line_buffer);
    }

    fclose(fp);
    return true;
}

// returns number of elements set in median_indices
size_t find_median_indices(const std::vector<size_t>& elements, size_t low, size_t high, size_t *median_indices) {
    size_t num_elements = high - low + 1;
    if (num_elements % 2 == 0) {
        size_t left_idx = (num_elements / 2) + low - 1;
        size_t right_idx = left_idx + 1;
        median_indices[0] = left_idx;
        median_indices[1] = right_idx;
        return 2;
    } else {
        size_t idx = (num_elements / 2) + low;
        median_indices[0] = idx;
        return 1;
    }
}

size_t avg(size_t *elems, size_t num_elems) {
    size_t total = 0;
    size_t idx = 0;
    while (idx < num_elems) {
        total += elems[idx];
        idx++;
    }
    size_t avg_value = total / num_elems;
    return avg_value;
}
