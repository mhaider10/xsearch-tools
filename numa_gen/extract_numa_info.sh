#!/bin/bash

lscpu --extended | awk '{if (NR > 1) { print $1","$2","$4; X+=1; }} {if ($1 > max && NR > 2) max = $2}' > numa_info.txt
numa_nodes=$(lscpu | awk 'BEGIN { ORS="," } /NUMA node\(s\)/ {print $(NF)}' | sed 's/.$//')
cores_per_socket=$(lscpu | awk 'BEGIN { ORS="," } /per socket/ {print $(NF)}' | sed 's/.$//')
threads_per_core=$(lscpu | awk 'BEGIN { ORS="," } /Thread\(s\) per core/ {print $(NF)}' | sed 's/.$//')
sockets=$(lscpu | awk 'BEGIN { ORS="," } /Socket\(s\)/ {print $(NF)}' | sed 's/.$//')
let "total_cores = $sockets * $cores_per_socket"
let "cores_per_numa = $total_cores / $numa_nodes"
formatted="${numa_nodes},${cores_per_numa},${threads_per_core}"
echo ${formatted} >> numa_info.txt
