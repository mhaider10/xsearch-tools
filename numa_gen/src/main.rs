const NUMA_STRATEGIES : &str = "<fcfn/fcsn/fcffn/fcfsn>";

fn main() {
    let args : Vec<String> = std::env::args().collect();
    let args_len : usize = args.len();
    if args.len() < 3 {
        println!("./numa_gen <info_path> {}", NUMA_STRATEGIES);
        std::process::exit(1);
    }
    let file_name : &String = &args[args_len - 2];
    let numa_strat : &String = &args[args_len - 1];

    let numa_strat : NUMAStrategy = match numa_strat.as_ref() {
        "fcfn" => NUMAStrategy::FillCoreFillNuma,
        "fcsn" => NUMAStrategy::FillCoreSpreadNuma,
        "fcffn" => NUMAStrategy::FillCoreFirstFillNuma,
        "fcfsn" => NUMAStrategy::FillCoreFirstSpreadNuma,
        _ => {
            println!("Strategy must be one of the following: {}", NUMA_STRATEGIES);
            std::process::exit(1);
        }
    };

    let file_contents : String = match std::fs::read_to_string(file_name) {
        Ok(contents) => contents,
        Err(_) => {
            println!("Couldn't read file {}", file_name);
            std::process::exit(1);
        }
    };
    let v : Vec<&str> = file_contents.split("\n").collect();
    let mut last_entry : usize = v.len() - 1;

    // Ignore trailing lines
    while v[last_entry].len() == 0 {
        last_entry -= 1;
    }
    let numa_info : &str = v[last_entry]; // Last line contains NUMA information (NUMA nodes, cores per NUMA node)
    let numa_info : Vec<&str> = numa_info.split(",").collect();
    let numa_node_count : u16 = parse_numa_info(numa_info[0], "a NUMA node count");
    let core_per_numa : u16 = parse_numa_info(numa_info[1], "a core per NUMA node count");
    let threads_per_core : u16 = parse_numa_info(numa_info[2], "a threads per core count");
    let mut hardware_thread_map : Vec<Vec<Vec<u16>>> = vec![vec![Vec::new(); core_per_numa as usize]; numa_node_count as usize];
    for i in 0..=last_entry-1 {
        let core_info : Vec<&str> = v[i].split(",").collect();
        let cpu_id : u16 = core_info[0].parse::<u16>().unwrap();
        let numa_index : u16 = core_info[1].parse::<u16>().expect("Could not parse NUMA index");
        let core_index : u16 = core_info[2].parse::<u16>().expect("Could not parse core index");
        hardware_thread_map[numa_index as usize][(core_index % core_per_numa) as usize].push(cpu_id);
    }

    println!("{}", gen_hardware_thread_sequence(numa_strat, &hardware_thread_map, threads_per_core));
}

struct CoreLayout {
    tokenizer_cores: Vec<CoreDescription>,
    producer_cores: Vec<CoreDescription>
}

impl std::fmt::Display for CoreLayout {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut tokenizer_str = String::new();
        for core in &self.tokenizer_cores {
            tokenizer_str += &core.to_string();
            tokenizer_str += ",";
        }
        let mut chars = tokenizer_str.chars();
        chars.next_back();
        let _ = write!(f, "{}\n", chars.as_str());
        let mut producer_str = String::new();
        for core in &self.producer_cores {
            producer_str += &core.to_string();
            producer_str += ",";
        }
        let mut chars = producer_str.chars();
        chars.next_back();
        let _ = write!(f, "{}", chars.as_str());
        Ok(())
    }
}

impl std::fmt::Debug for CoreLayout {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Tokenizer Cores: {:?}\n", self.tokenizer_cores).expect("Failed to write tokenizers");
        write!(f, "Producer Cores: {:?}\n", self.producer_cores).expect("Failed to write producers");
        Ok(())
    }
}

struct CoreDescription {
    cpu_id: u16,
    numa_id: u16
}

impl ToString for CoreDescription {
    fn to_string(&self) -> String {
        format!("{}|{}", self.cpu_id.to_string(), self.numa_id.to_string())
    }
}

impl std::fmt::Debug for CoreDescription {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "(cpu_id: {}, numa_id: {})", self.cpu_id.to_string(), self.numa_id.to_string()).expect("Failed to write CoreTokenizer");
        Ok(())
    }
}

fn gen_hardware_thread_sequence(strategy: NUMAStrategy, hardware_thread_map: &Vec<Vec<Vec<u16>>>, hyperthread_max: u16) -> CoreLayout {
    let mut tokenizer_cores : Vec<CoreDescription> = Vec::new();
    let mut producer_cores : Vec<CoreDescription> = Vec::new();
    let mut tokenizer_toggle : bool = false;

    match strategy {
        NUMAStrategy::FillCoreFillNuma =>  {
            let mut numa_id : u16 = 0;
            for core_vector in hardware_thread_map {
                for hyperthread_vector in core_vector {
                    for hyperthread_index in 0usize..(hyperthread_max as usize) {
                        let vec_ref : &mut Vec<CoreDescription> = if tokenizer_toggle { &mut tokenizer_cores } else { &mut producer_cores };
                        vec_ref.push(CoreDescription { cpu_id: hyperthread_vector[hyperthread_index], numa_id});
                        tokenizer_toggle = !tokenizer_toggle;
                    }
                }
                numa_id += 1;
            }
            CoreLayout { tokenizer_cores, producer_cores }
        }
        NUMAStrategy::FillCoreFirstFillNuma =>  {
            for hyperthread_index in 0usize..(hyperthread_max as usize) {
                let mut numa_id : u16 = 0;
                for core_vector in hardware_thread_map {
                    for hyperthread_vector in core_vector {
                        let vec_ref : &mut Vec<CoreDescription> = if tokenizer_toggle { &mut tokenizer_cores } else { &mut producer_cores };
                        vec_ref.push(CoreDescription { cpu_id: hyperthread_vector[hyperthread_index], numa_id});
                        tokenizer_toggle = !tokenizer_toggle;
                    }
                    numa_id += 1;
                }
            }
            CoreLayout { tokenizer_cores, producer_cores }
        }
        NUMAStrategy::FillCoreSpreadNuma =>  {
            for core_index in 0usize..(hardware_thread_map[0].len()) {
                for hyperthread_index in 0usize..(hyperthread_max as usize) {
                    for numa_index in 0usize..(hardware_thread_map.len()) {
                        let vec_ref : &mut Vec<CoreDescription> = if tokenizer_toggle { &mut tokenizer_cores } else { &mut producer_cores };
                        vec_ref.push(CoreDescription { cpu_id: hardware_thread_map[numa_index][core_index][hyperthread_index], numa_id: numa_index as u16});
                    }
                    tokenizer_toggle = !tokenizer_toggle;
                }
            }
            CoreLayout { tokenizer_cores, producer_cores }
        }
        NUMAStrategy::FillCoreFirstSpreadNuma =>  {
            for hyperthread_index in 0usize..(hyperthread_max as usize) {
                for core_index in 0usize..(hardware_thread_map[0].len()) {
                    for numa_index in 0usize..(hardware_thread_map.len()) {
                        let vec_ref : &mut Vec<CoreDescription> = if tokenizer_toggle { &mut tokenizer_cores } else { &mut producer_cores };
                        vec_ref.push(CoreDescription { cpu_id: hardware_thread_map[numa_index][core_index][hyperthread_index], numa_id: numa_index as u16});
                    }
                    tokenizer_toggle = !tokenizer_toggle;
                }
            }
            CoreLayout { tokenizer_cores, producer_cores }
        }
    }
}

enum NUMAStrategy {
    FillCoreFillNuma,
    FillCoreSpreadNuma,
    FillCoreFirstFillNuma,
    FillCoreFirstSpreadNuma
}

fn parse_numa_info<T: std::str::FromStr>(numa_value : &str, purpose : &str) -> T {
    match numa_value.parse::<T>() {
        Ok(numa_nodes) => numa_nodes,
        Err(_) => {
            println!("Couldn't understand {} as {}", numa_value, purpose);
            std::process::exit(1);
        }
    }
}
