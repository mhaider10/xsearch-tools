#include "synthetic.h"
#include <string.h>

void SyntheticGenerator::generateDataset() {
    this->openFile();
    char synthetic_word_buffer[this->token_size];
    memset(synthetic_word_buffer, ' ', this->token_size);
    int counter = 0;
    while (true) {
        auto c = 'A' + (char) (counter % 26);
        memset(synthetic_word_buffer, c, this->token_size - 1);
        bool succeeded = this->dumpRecord(synthetic_word_buffer, this->token_size);
        if (!succeeded) break;
        counter++;
    }
    this->closeFile();
}
