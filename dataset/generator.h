#ifndef DATASET_BASE_GENERATOR
#define DATASET_BASE_GENERATOR

#include <stdio.h>
#include <string>

class BaseGenerator {
    public:
        BaseGenerator(std::string filepath, long total_bytes) : filepath(filepath), bytes_written(0), total_bytes(total_bytes) {}
        virtual void loadDataset() {}
        virtual void generateDataset() = 0;

        virtual void openFile() final;
        virtual void closeFile() final;

        virtual bool dumpRecord(void *buf, long bytes) final; 
        virtual bool checkComplete() final;

        virtual ~BaseGenerator() {}
    private:
        std::string filepath;
        long bytes_written;
        long total_bytes;
        FILE* fp;
};

#endif
