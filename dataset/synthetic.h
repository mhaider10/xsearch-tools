#ifndef DATASET_SYNTHETIC_GENERATOR
#define DATASET_SYNTHETIC_GENERATOR

#include "generator.h"

class SyntheticGenerator : public BaseGenerator {
    private:
        unsigned int token_size;
    public:
        SyntheticGenerator(std::string filepath, long total_bytes, unsigned int token_size) : BaseGenerator(filepath, total_bytes), token_size(token_size) {}
        void generateDataset();
};

#endif
