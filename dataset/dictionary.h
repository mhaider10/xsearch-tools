#ifndef DATASET_DICTIONARY_GENERATOR
#define DATASET_DICTIONARY_GENERATOR

#include "generator.h"

class DictionaryGenerator : public BaseGenerator {
    public:
        DictionaryGenerator(std::string filepath, long total_bytes, std::string input_filepath) : BaseGenerator(filepath, total_bytes), input_filepath(input_filepath) {}
        void generateDataset();
        void loadDataset();
    private:
        std::string input_filepath;
};

#endif
