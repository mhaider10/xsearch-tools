#include "generator.h"
#include <iostream>

void BaseGenerator::openFile() {
    this->fp = fopen(this->filepath.c_str(), "w+");
    if (this->fp == NULL) std::cout << "ERR: cannot open file " << this->filepath << std::endl;
}

void BaseGenerator::closeFile() {
    fclose(this->fp);
}

bool BaseGenerator::dumpRecord(void *buf, long bytes) {
    long max_bytes_left = this->total_bytes - this->bytes_written;
    if (max_bytes_left == 0 || bytes == 0) return false;
    bytes = std::min(bytes, max_bytes_left);
    long file_bytes_written = fwrite(buf, bytes, 1, this->fp) * bytes;
    if (file_bytes_written != bytes) std::cout << "Only wrote " << file_bytes_written << " bytes, but expected to write " << bytes << std::endl;
    this->bytes_written += file_bytes_written;
    return true;
}

bool BaseGenerator::checkComplete() {
    return this->bytes_written >= this->total_bytes;
}
