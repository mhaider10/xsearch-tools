#include "dictionary.h"
#include "synthetic.h"

#include <cstring>
#define MSG "./dataset_generator <synthetic/dictionary> <output filename> <size>"

#include <iostream>
#include <string>
#include <stdlib.h>
#include "generator.h"

inline void print_extra_arguments(const std::string& generator_type, const std::string& extra_args) {
    std::cout << "'" << generator_type << "' generator requires extra arguments" << std::endl;
    std::cout << MSG << " " << extra_args;
}

int main(int argc, char **argv) {
    std::string using_generator = "synthetic"; // default generator

    BaseGenerator* generator;

    if (argc < 4) {
        std::cout << MSG << std::endl;
        return 1;
    }

    using_generator = argv[1];
    std::string filepath(argv[2]);
    long filesize(atol(argv[3]));

    if (!using_generator.compare("dictionary")) {
        if (argc < 5) {
            print_extra_arguments("dictionary", "<input file>");
            return 1;
        }
        char *input_filename = argv[4];
        generator = new DictionaryGenerator(filepath, filesize, input_filename);
    } else {
        if (argc < 5) {
            print_extra_arguments("synthetic", "<token size>");
            return 1;
        }
        int token_size = atoi(argv[4]);
        generator = new SyntheticGenerator(filepath, filesize, token_size);
    }


    generator->loadDataset();
    generator->generateDataset();
    delete generator;
    return 0;
}
