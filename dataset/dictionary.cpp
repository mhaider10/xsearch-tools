#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "dictionary.h"

#define ENGLISH_WORD_COUNT 9884

char** english_words;

char* get_random_english_word()
{
    return english_words[std::rand() % ENGLISH_WORD_COUNT];
}

int load_english_words(const char *filepath)
{
    english_words = new char*[16384];
    FILE *fp = fopen(filepath, "r+");
    if (!fp) return -1;
    char words[262144];
    fseek(fp, 0, SEEK_END);
    long fSize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    int read_members = fread(words, 1, fSize, fp);
    char *token;
    char *iterator = words;
    int counter = 0;
    while ((token = strtok_r(iterator, "\n", &iterator))) {
        int tokenSize = strlen(token) + 1;
        char* allocWord = new char[tokenSize];
        std::memcpy(allocWord, token, tokenSize);
        english_words[counter++] = allocWord;
    }
    return read_members;
}

void DictionaryGenerator::loadDataset() {
    std::srand(time(NULL));
    load_english_words(this->input_filepath.c_str());
}

void DictionaryGenerator::generateDataset() {
    this->openFile();
    char *english_word;
    int word_len;
    bool didWrite;
    char space = ' ';
    while (true) {
        english_word = get_random_english_word();
        word_len = strlen(english_word);
        didWrite = this->dumpRecord(english_word, word_len);
        if (!didWrite) break;
        didWrite = this->dumpRecord(&space, 1);
        if (!didWrite) break;
    }
    this->closeFile();
}
